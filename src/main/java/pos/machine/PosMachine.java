package pos.machine;

import java.util.List;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = Item.decodeToltems(barcodes);
        ReceiptItem receiptItem = new ReceiptItem();
        Receipt receipt = receiptItem.calculateCost(receiptItems);
        return receipt.renderReceipt(receipt);
    }
}
