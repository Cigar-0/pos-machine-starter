package pos.machine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ItemsLoader {
    public static List<Item> loadAllItems() {
        Item item1 = new Item("ITEM000000", "Coca-Cola", 3);
        Item item2 = new Item("ITEM000001", "Sprite", 3);
        Item item3 = new Item("ITEM000004", "Battery", 2);
        List<Item> items = new ArrayList<>();
        items.add(item1);
        items.add(item2);
        items.add(item3);

        return items;
    }
    public static void main(String[] args) {
        PosMachine posMachine = new PosMachine();
        System.out.println(posMachine.printReceipt(loadBarcodes()));
    }

    private static List<String> loadBarcodes() {
        return Arrays.asList("ITEM000000", "ITEM000000", "ITEM000000", "ITEM000000", "ITEM000001", "ITEM000001", "ITEM000004", "ITEM000004", "ITEM000004");
    }
}
