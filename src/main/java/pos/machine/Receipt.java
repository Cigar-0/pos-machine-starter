package pos.machine;

import java.util.List;

public class Receipt {
    private List<ReceiptItem> receiptItems;
    private int totalPrice;

    public Receipt(List<ReceiptItem> receiptItems, int totalPrice) {
        this.receiptItems = receiptItems;
        this.totalPrice = totalPrice;
    }


    public String generateltemsReceipt(Receipt receipt){
        StringBuilder sb=new StringBuilder();
        for (ReceiptItem r:receipt.getReceiptItems()
        ) {
            sb.append(r.toString());
        }
        return sb.toString();
    }
    public String renderReceipt(Receipt receipt){
        return "***<store earning no money>Receipt***\n" + generateltemsReceipt(receipt) + generateReceipt(receipt);
    }
    public String generateReceipt(Receipt receipt){
        return  "----------------------\n" +
                "Total: " + receipt.getTotalPrice() +" (yuan)\n" +
                "**********************";
    }

    public List<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(List<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }
}
