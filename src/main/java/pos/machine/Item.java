package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Item {
    private final String barcode;
    private final String name;
    private final int price;

    public Item(String barcode, String name, int price) {
        this.barcode = barcode;
        this.name = name;
        this.price = price;
    }
    public static List<ReceiptItem> decodeToltems(List<String> barcodes){
        List<Item> allItems = ItemsLoader.loadAllItems();
        Map<String, Item> itemMap = new HashMap<>();
        for (Item item : allItems) {
            itemMap.put(item.getBarcode(), item);
        }

        List<Item> items = new ArrayList<>();
        for (String barcode : barcodes) {
            items.add(itemMap.get(barcode));
        }

        Map<String, ReceiptItem> receiptItemMap = new HashMap<>();

        for (Item item : items) {
            String name = item.getName();
            int price = item.getPrice();
            if (!receiptItemMap.containsKey(name)) {
                receiptItemMap.put(name, new ReceiptItem(name, 1, price, price));
            } else {
                ReceiptItem existingItem = receiptItemMap.get(name);
                existingItem.setQuantity(existingItem.getQuantity() + 1);
                existingItem.setSubTotal(existingItem.getQuantity() * price);
            }
        }

        return new ArrayList<>(receiptItemMap.values());
    }


    public String getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }
}
