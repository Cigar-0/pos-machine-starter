package pos.machine;

import java.util.List;

public class ReceiptItem {
    private String name;
    private int quantity;
    private int unitPrice;
    private  int subTotal;
    public ReceiptItem(String name, int quantity, int unitPrice, int subTotal) {
        this.name = name;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.subTotal = subTotal;
    }
    public ReceiptItem() {

    }
    public Receipt calculateCost(List<ReceiptItem> receiptltemList){
        return new Receipt(receiptltemList,calculateTotalPrice(receiptltemList));
    }
    public int calculateTotalPrice(List<ReceiptItem> receiptltemList) {
        int totalPrice=0;
        for (ReceiptItem receiptItem:receiptltemList
        ) {
            totalPrice+=receiptItem.getSubTotal();
        }
        return totalPrice;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(int unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(int subTotal) {
        this.subTotal = subTotal;
    }
    @Override
    public String toString() {
        return  "Name: " + name  +
                ", Quantity: " + quantity +
                ", Unit price: "+ unitPrice +
                " (yuan), Subtotal: " + subTotal +
                " (yuan)\n";
    }
}
